/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Calixte DENIZET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.forge.scidoc;

import java.io.InputStream;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Class the convert a DocBook xml file
 * @author Calixte DENIZET
 */
public class HTMLDocbookLinkResolver extends DefaultHandler {

    private Map<String, String> mapId = new LinkedHashMap();
    private Map<String, String> toc = new LinkedHashMap();
    private Map<String, TreeId> mapTreeId = new HashMap();
    private TreeId tree = new TreeId(null, "root");

    private TreeId currentLeaf = tree;

    private String current;
    private String lastId;
    private Locator locator;
    private String currentFileName;
    private boolean waitForRefname;
    private boolean waitForTitle;
    private boolean getContents;
    private String inName;
    private int level;
    private StringBuilder buffer = new StringBuilder(256);
    private InputStream in;
    
    /**
     * Constructor
     * @param in the inputstream
     * @param inName the name of the input file
     * @param out the outputstream
     */
    public HTMLDocbookLinkResolver(String inName) throws IOException, SAXException {
	this.in = new FileInputStream(new File(inName));
	this.inName = inName;
	resolvLinks();
    }

    /**
     * @return the map id
     */
    public Map<String, String> getMapId() {
	return mapId;
    }

    /**
     * @return the tocs
     */
    public Map<String, String> getToc() {
	return toc;
    }

    /**
     * @return the document tree
     */
    public TreeId getTree() {
	return tree;
    }

    /**
     * @return the document tree
     */
    public Map<String, TreeId> getMapTreeId() {
	return mapTreeId;
    }

    /**
     * {@inheritDoc}
     */
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
	currentFileName = systemId;
	return super.resolveEntity(publicId, systemId);
    }

    /**
     * {@inheritDoc}
     */
    public void startDocument() throws SAXException { }

    /**
     * {@inheritDoc}
     */
    public void endDocument() throws SAXException { }

    /**
     * {@inheritDoc}
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	int len = attributes.getLength();
	String id = null;
	for (int i = 0; i < len; i++) {
	    if (attributes.getLocalName(i).equals("id")) {
		id = attributes.getValue(i);
		break;
	    }
	}

	if (localName.equals("title")) {
	    if (waitForTitle) {
		getContents = true;
		buffer.setLength(0);
	    }
	} else if (localName.equals("refname")) {
	    if (waitForRefname) {
		getContents = true;
		buffer.setLength(0);
	    }
	} else if (localName.equals("refentry") || localName.equals("section") || localName.equals("part") || localName.equals("chapter")) {
	    if (id == null) {
		throw new SAXException(errorMsg());
	    }
	    current = id + ".html";
	    lastId = id;
	    mapId.put(id, current);
	    waitForTitle = localName.charAt(0) != 'r';
	    waitForRefname = !waitForTitle;
	    TreeId leaf = new TreeId(currentLeaf, id);
	    currentLeaf.add(leaf);
	    currentLeaf = leaf;
	} else if (id != null && current != null) {
	    mapId.put(id, current + "#" +id);
	}
    }

    /**
     * {@inheritDoc}
     */
    public void endElement(String uri, String localName, String qName) throws SAXException {
	if (getContents) {
	    toc.put(lastId, buffer.toString().trim());
	    getContents = false;
	    waitForRefname = false;
	    waitForTitle = false;
	}
	if (localName.equals("refentry") || localName.equals("section") || localName.equals("part") || localName.equals("chapter")) {
	    currentLeaf = currentLeaf.parent;
	}
    }

    /**
     * {@inheritDoc}
     */
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    /**
     * {@inheritDoc}
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
	if (getContents) {
	    int end = start + length;
	    int save = start;
	    for (int i = start; i < end; i++) {
		switch (ch[i]) {
		case '\'' :
		    buffer.append(ch, save, i - save);
		    buffer.append("&apos;");
		    save = i + 1;
		    break;
		case '\"' :
		    buffer.append(ch, save, i - save);
		    buffer.append("&quot;");
		    save = i + 1;
		    break;
		case '<' :
		    buffer.append(ch, save, i - save);
		    buffer.append("&lt;");
		    save = i + 1;
		    break;
		case '>' :
		    buffer.append(ch, save, i - save);
		    buffer.append("&gt;");
		    save = i + 1;
		    break;
		case '&' :
		    buffer.append(ch, save, i - save);
		    buffer.append("&amp;");
		    save = i + 1;
		    break;
		default :
		    break;
		}
	    }
	    
	    if (save < end) {
		buffer.append(ch, save, end - save);
	    }
	}
    }

    /**
     * Start the conversion
     * @throws SAXException if a problem is encountered during the parsing
     * @throws IOException if an IO problem is encountered
     */
    protected void resolvLinks() throws SAXException, IOException {
	SAXParserFactory factory = SAXParserFactory.newInstance();
	factory.setValidating(false);
	factory.setNamespaceAware(true);

	try {
	    SAXParser parser = factory.newSAXParser();
            parser.parse(in, this);
	    in.close();
        } catch (ParserConfigurationException e) {
	    System.err.println(e);
	}
    }

    /**
     * @return the errror message
     */
    private String errorMsg() {
	String str = inName;
	if (currentFileName != null) {
	    str = currentFileName;
	} 

	return "Refentry without id attributes in file " + str + " at line " + locator.getLineNumber();
    }

    class TreeId {

	String id;
	TreeId parent;
	int pos;
	List<TreeId> children;

	TreeId(TreeId parent, String id) {
	    this.parent = parent;
	    this.id = id;
	    if (parent == null) {
		mapTreeId.clear();
	    }
	    mapTreeId.put(id, this);
	}

	void add(TreeId child) {
	    if (children == null) {
		children = new ArrayList();
	    }
	    child.pos = children.size();
	    children.add(child);
	}

	boolean isRoot() {
	    return parent == null;
	}

	TreeId getPrevious() {
	    if (pos > 0) {
		return parent.children.get(pos - 1);
	    }
	    return parent;
	}

	TreeId getNext() {
	    TreeId l = this;
	    while (l.parent != null && l.pos == l.parent.children.size() - 1) {
		l = l.parent;
	    }
	    
	    if (l.parent == null) {
		return null;
	    }
	    return l.parent.children.get(l.pos + 1);
	}
    }
}
