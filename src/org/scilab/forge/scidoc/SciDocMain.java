/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2010 - Calixte DENIZET
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

package org.scilab.forge.scidoc;

import java.util.HashMap;
import java.util.Map;

import org.scilab.forge.scidoc.external.HTMLMathMLHandler;
import org.scilab.forge.scidoc.external.HTMLSVGHandler;

/**
 * Class to convert DocBook to HTML
 * @author Calixte DENIZET
 */
public final class SciDocMain {

    public static void main(String[] args) {
	Map<String, String> map = parseCommandLine(args);
	if (map.containsKey("help")) {
	    System.out.println("Usage scidoc [OPTION]... file");
	    System.out.println("SciDoc is a tool to generate html or javahelp files from DocBook");
	    System.out.println("");
	    System.out.println("-input        Input DocBook file");
	    System.out.println("-output       The output directory");
	    System.out.println("-template     A template file used for the generation");
	    System.out.println("-imagedir     The directory which will contain the generated images");
	    System.out.println("-javahelp     No expected argument, just precise the kind of output");
	    System.out.println("-html         No expected argument, just precise the kind of output (default)");
	    System.out.println("-sciprim      A file containing the list of the Scilab primitives");
	    System.out.println("-scimacro     A file containing the list of the Scilab macros");
	    System.out.println("-version      A string with the version of Scilab");
	    //System.out.println("-checklast    true or false, just say if the date of the last generation");
	    //System.out.println("              must be checked, it could be useful to regenerate only the changed files");
	    System.out.println("");
	    System.out.println("Report bugs on: <http://bugzilla.scilab.org>");
	    System.out.println("Project page: <http://forge.scilab.org/index.php/p/scidoc>");
	    return;
	}

	if (!map.containsKey("input")) {
	    System.err.println("No input file");
	    System.err.println("Use the option -help");
	    return;
	}

	if (!map.containsKey("template")) {
	    System.err.println("No template to generate files");
	    System.err.println("Use the option -help");
	    return;
	}

	if (!map.containsKey("imagedir")) {
	    System.err.println("No image directory specified");
	    System.err.println("Use the option -help");
	    return;
	}

	String input = map.get("input");
	String output = map.get("output");
	if (output == null || output.length() ==0) {
	    output = ".";
	}
	String template = map.get("template");
	String sciprim = map.get("sciprim");
	String scimacro = map.get("scimacro");
	String version = map.get("version");
	String imagedir = map.get("imagedir");
	boolean checkLast = false;//Boolean.parseBoolean(map.get("checklast"));

	try {
	    DocbookTagConverter converter;
	    if (map.containsKey("javahelp")) {
		converter = new JavaHelpDocbookTagConverter(input, output, sciprim, scimacro, template, version, imagedir, checkLast);
	    } else {
		converter = new HTMLDocbookTagConverter(input, output, sciprim, scimacro, template, version, imagedir, checkLast);
	    }
	    converter.registerExternalXMLHandler(new HTMLMathMLHandler(imagedir));
	    converter.registerExternalXMLHandler(new HTMLSVGHandler(imagedir));
	    converter.convert();
	} catch (Exception e) {
	    System.err.println("An error occured during the conversion:\n");
	    e.printStackTrace();
	}
    }

    private static Map<String, String> parseCommandLine(String[] args) {
	String option = null;
	boolean in = false;
	Map<String, String> map = new HashMap();
	for (int i = 0; i < args.length; i++) {
	    if (args[i].length() >= 2 && args[i].charAt(0) == '-') {
		if (option != null) {
		    map.put(option, "");
		    option = null;
		    option = args[i];
		}
		if (args[i].charAt(1) == '-') {
		    option = args[i].substring(2);
		} else {
		    option = args[i].substring(1);
		}
	    } else {
		if (option != null) {
		    map.put(option, args[i]);
		    option = null;
		} else if (!in) {
		    map.put("input", args[i]);
		    in = true;
		} else {
		    System.err.println("Not an argument " + args[i]);
		    return null;
		}
	    }
	}
	if (option != null) {
	    map.put(option, "");
	}

	return map;
    }
}
